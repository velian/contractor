﻿using UnityEngine;
using System.Collections;

public class FadeOut : MonoBehaviour {

    public Texture m_black;

    public float m_speed = 1;
    protected float m_alpha = 0;

    void Start()
    {
        Fade(0);
    }

    void OnGUI()
    {
        GUI.color = new Color(0, 0, 0, m_alpha);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), m_black);
    }

    public void Fade(int _sceneNumber = -1)
    {
        StartCoroutine(EFadeOut(_sceneNumber));
    }
    
    IEnumerator EFadeOut(int _sceneNumber)
    {
        for (float i = 0; i < 1; )
        {
            i += Time.deltaTime * m_speed;
            m_alpha = i;
            yield return null;
        }

        if (_sceneNumber == -1)
            yield return null;
        else
            Application.LoadLevel(_sceneNumber);
    }
}
