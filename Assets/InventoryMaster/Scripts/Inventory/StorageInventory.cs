﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class StorageInventory : MonoBehaviour
{

    [SerializeField]
    public GameObject inventory;

    [SerializeField]
    public List<Item> storageItems = new List<Item>();

    [SerializeField]
    private ItemDataBaseList itemDatabase;

    [SerializeField]
    public int distanceToOpenStorage;

    public float timeToOpenStorage;

    private InputManager inputManagerDatabase;

    public int itemAmount;

    public Color colourSelected = Color.green;

    Inventory inv;

    GameObject player;
    GameObject mainInventoryRef;

    bool closeInv;

    bool showStorage;

    public void addItemToStorage(int id, int value)
    {
        Item item = itemDatabase.getItemByID(id);
        item.itemValue = value;
        storageItems.Add(item);
    }

    void Start()
    {
        if (inputManagerDatabase == null)
            inputManagerDatabase = (InputManager)Resources.Load("InputManager");

        player = GameObject.FindGameObjectWithTag("Player");

        inventory = GetPanels.FindInactiveWithTag("Storage");
        mainInventoryRef = GetPanels.FindInactiveWithTag("MainInventory");

        inv = inventory.GetComponent<Inventory>();
        ItemDataBaseList inventoryItemList = (ItemDataBaseList)Resources.Load("ItemDatabase");

        int creatingItemsForChest = 1;

        int randomItemAmount = Random.Range(1, itemAmount);

        while (creatingItemsForChest < randomItemAmount)
        {
            int randomItemNumber = Random.Range(1, inventoryItemList.itemList.Count - 1);
            int raffle = Random.Range(1, 100);

            if (raffle <= inventoryItemList.itemList[randomItemNumber].rarity)
            {
                int randomValue = Random.Range(1, inventoryItemList.itemList[randomItemNumber].getCopy().maxStack);
                Item item = inventoryItemList.itemList[randomItemNumber].getCopy();
                item.itemValue = randomValue;
                storageItems.Add(item);
                creatingItemsForChest++;
            }
        }
    }

    public void setImportantVariables()
    {
        if (itemDatabase == null)
            itemDatabase = (ItemDataBaseList)Resources.Load("ItemDatabase");
    }

    void Update()
    {
        float distance = Vector3.Distance(this.gameObject.transform.position, player.transform.position);

        if (distance <= distanceToOpenStorage && !showStorage && mainInventoryRef.active)
        {
            showStorage = true;
            StartCoroutine(OpenInventoryWithTimer());
            GetComponent<SpriteRenderer>().material.color = colourSelected;
        }
        else if (distance <= distanceToOpenStorage && !showStorage && !mainInventoryRef.active)
        {
            GetComponent<SpriteRenderer>().material.color = Color.yellow;
        }
        else if (distance > distanceToOpenStorage && showStorage || !mainInventoryRef.active)
        {
            showStorage = false;
            if (inventory.activeSelf)
            {
                storageItems.Clear();
                setListofStorage();
                inventory.SetActive(false);
                inv.deleteAllItems();
                GetComponent<SpriteRenderer>().material.color = Color.white;
            }            
        }

        if (distance > distanceToOpenStorage && !showStorage && GetComponent<SpriteRenderer>().material.color == Color.yellow)
        {
            GetComponent<SpriteRenderer>().material.color = Color.white;
        }
    }

    IEnumerator OpenInventoryWithTimer()
    {
        if (showStorage)
        {
            yield return new WaitForSeconds(timeToOpenStorage);
            if (showStorage)
            {
                inv.ItemsInInventory.Clear();
                inventory.SetActive(true);
                addItemsToInventory();
            }
        }
        else
        {
            storageItems.Clear();
            setListofStorage();
            inventory.SetActive(false);
            inv.deleteAllItems();
        }


    }



    void setListofStorage()
    {
        Inventory inv = inventory.GetComponent<Inventory>();
        storageItems = inv.getItemList();
    }

    void addItemsToInventory()
    {
        Inventory iV = inventory.GetComponent<Inventory>();
        for (int i = 0; i < storageItems.Count; i++)
        {
            iV.addItemToInventory(storageItems[i].itemID, storageItems[i].itemValue);
        }
        iV.stackableSettings();
    }






}
