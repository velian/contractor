﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmmunitionUI : MonoBehaviour
{
    GameObject m_player;
    public List<BulletIcon> m_bulletList;

    public GameObject m_bulletIconPrefab;    
    public float m_offset = 0.25f;
    public int m_capacity = 42;

	// Use this for initialization
	void Start()
    {
        m_player = GameObject.FindWithTag("Player");
        m_bulletList = new List<BulletIcon>();
        InitializeClip();
	}

    public void InitializeClip()
    {
        Vector3 offset = Vector3.zero;
        m_bulletList.Clear();

        for (int i = 0; i < m_player.GetComponentInChildren<WeaponBase>().m_clip; i++)
        {
            GameObject bullet = (GameObject)Instantiate(m_bulletIconPrefab);
            bullet.transform.SetParent(GetComponent<RectTransform>().transform);

            RectTransform rectTrans = bullet.GetComponent<RectTransform>();

            rectTrans.position = transform.position;
            rectTrans.anchoredPosition = new Vector2(-4.8f, 1.2f);

            //Offset if we have a huge fucking magazine
            if (i < m_capacity)
                rectTrans.anchoredPosition += new Vector2(i % m_capacity * m_offset, 0);
            else
                rectTrans.anchoredPosition += new Vector2(i % m_capacity * m_offset, -2.2f);

            rectTrans.localScale = new Vector3(0.05f, 0.1f, 1f);
            bullet.GetComponentInChildren<BulletIcon>().PlayAddAnimation();

            m_bulletList.Add(bullet.GetComponentInChildren<BulletIcon>());
        }
    }

    public void ClearChildren()
    {
        foreach (BulletIcon child in m_bulletList)
        {
            if (child)
            {
                Destroy(child.transform.parent.gameObject);
            }
        }
    }

    public void RemoveBullet()
    {
        if (m_bulletList.Count > 0)
        {
            m_bulletList[m_bulletList.Count - 1].GetComponentInChildren<BulletIcon>().PlayRemoveAnimation();
            m_bulletList.RemoveAt(m_bulletList.Count - 1);
        }
    }
}
