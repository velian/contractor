﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public Vector3 m_direction;
    public GameObject m_spatterPrefab;
    public float m_speed;
    public float m_timer = 3;
    public string m_owner;
    public float m_damage;

	// Use this for initialization
	void Start()
    {
        GetComponent<Rigidbody>().AddForce(m_direction * m_speed * Time.deltaTime, ForceMode.Impulse);
	}
	
	// Update is called once per frame
	void Update()
    {
        m_timer -= Time.deltaTime;
        if(m_timer < 0)
        {
            Destroy(this.gameObject);
        }
	}

    public void DestroyBullet()
    {
        Instantiate(m_spatterPrefab, transform.position, new Quaternion(0, 1, 0, 1));
        Destroy(this.gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Gun") 
        {
            Physics.IgnoreCollision(collision.collider, GetComponent<Collider>());
            return;
        }

        //Spawn bullet spatter.
        DestroyBullet();
    }
}
