﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BulletIcon : MonoBehaviour {

    public float xForceMin;
    public float xForceMax;
    public float yForce;
    public float torque;
    public float gravityScale;

    public void PlayAddAnimation()
    {
        GetComponentInParent<Animator>().Play("Add");
    }

    public void PlayRemoveAnimation()
    {
        gameObject.AddComponent<Rigidbody2D>();
        Destroy(gameObject.GetComponentInParent<Animator>());
        Rigidbody2D rigid = GetComponent<Rigidbody2D>();
        rigid.AddForce(new Vector2(Random.Range(xForceMin, xForceMax), yForce), ForceMode2D.Impulse);
        rigid.AddTorque(Random.Range(-torque, torque));
        rigid.gravityScale = gravityScale;

        StartCoroutine(EHideBullet());
    }

    IEnumerator EHideBullet()
    {
        yield return new WaitForSeconds(0.5f);

        Color color = GetComponent<Image>().color;

        for (float i = 1; i > 0; )
        {
            i -= Time.deltaTime;
            GetComponent<Image>().color = new Color(color.r, color.g, color.b, i);
            yield return new WaitForEndOfFrame();
        }

        Destroy(transform.parent.gameObject);

        yield return null;
    }
}
