﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public Transform m_player;
    public float m_smoothness = 1;

    Vector3 m_velocity = Vector3.zero;

	// Use this for initialization
	void Start()
    {
	    
	}
	
	// Update is called once per frame
	void Update()
    {
        //Vector3 cameraOld = transform.position;
        //Vector3 cameraNew = new Vector3(m_player.position.x + cameraOld.x, 0, m_player.position.z + cameraOld.z);
        //
        //float camDistance = Vector3.Distance(cameraOld, cameraNew);
        //
        //transform.position = Vector3.Lerp(cameraOld, cameraNew, camDistance * m_smoothness);

        Vector3 point = Camera.main.WorldToViewportPoint(m_player.position);
        Vector3 delta = m_player.position - Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
        Vector3 destination = transform.position + delta;
        transform.position = Vector3.SmoothDamp(transform.position, new Vector3(destination.x, 5, destination.z), ref m_velocity, m_smoothness);
	}
}
