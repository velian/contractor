﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConnectionGenerator : MonoBehaviour
{
    public List<GameObject> m_possibleRooms;
    List<Transform> m_connectionPoints;

	// Use this for initialization
	void Start()
    {
        List<Transform> m_connectionPoints = new List<Transform>();
        GameObject roomPrefab = Resources.Load<GameObject>("Prefabs/Room");

        if (GenerationManager.m_currentIterations > GenerationManager.m_iterations) return;

        if(GenerationManager.m_currentIterations == 0)
        {
            roomPrefab.GetComponent<Room>().m_startingRoom = true;
            GameObject room = (GameObject)Instantiate(roomPrefab, roomPrefab.GetComponent<Transform>().position, roomPrefab.GetComponent<Transform>().rotation);

            string connectionName;

            for(int i = 0; i < 4; i++)
            {
                connectionName = string.Concat("Connection", (i + 1));
                Transform connection = room.GetComponent<Transform>().FindChild(connectionName);

                roomPrefab.GetComponent<Room>().m_startingRoom = false;
                roomPrefab.GetComponent<Room>().m_hideOnAwake = false;
                GameObject connectedRoom = (GameObject)Instantiate(roomPrefab, connection.position, connection.rotation);
                GenerationManager.m_currentIterations++;
            }
        }
        else
        {
            roomPrefab.GetComponent<Room>().m_hideOnAwake = true;
            GameObject room = (GameObject)Instantiate(roomPrefab, roomPrefab.GetComponent<Transform>().position, roomPrefab.GetComponent<Transform>().rotation);
        }
        GenerationManager.m_currentIterations++;        
	}
	
	// Update is called once per frame
	void Update()
    {
	
	}
}
