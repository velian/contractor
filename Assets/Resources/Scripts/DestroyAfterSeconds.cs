﻿using UnityEngine;
using System.Collections;

public class DestroyAfterSeconds : MonoBehaviour {

    [System.Serializable]
    public class Destructor
    {
        public float m_seconds;
        public Component[] m_componentsToDestroy;
    }

    public bool m_destroyAfterTime = false;
    public float m_destroyTime;

    public Destructor[] m_componentsToDestroy;

	// Use this for initialization
	void Start ()
    {
        if (m_componentsToDestroy.Length == 0)
        {
            print("Destroy After Seconds initialized with no object to destroy.");
        }        
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (m_destroyAfterTime && m_destroyTime < 0)
        {
            Destroy(this.gameObject);
        }
        else
        {
            m_destroyTime -= Time.deltaTime;

            for (int i = 0; i < m_componentsToDestroy.Length; i++)
            {
                m_componentsToDestroy[i].m_seconds -= Time.deltaTime;

                if (m_componentsToDestroy[i].m_seconds < 0)
                {
                    foreach (Component component in m_componentsToDestroy[i].m_componentsToDestroy)
                        Destroy(component);
                }
            } 
        }        
	}
}
