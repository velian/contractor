﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour
{
    Animator m_animator;
    bool triggered = false;

    void Start()
    {
        m_animator = GetComponent<Animator>();
    }

	// Update is called once per frame
    void OnTriggerEnter(Collider collision)
    {
        if (!triggered)
        {
            if (collision.tag == "Player")
            {
                //Play anim
                GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
                m_animator.Play("Open");
                triggered = true;
            }
        }
    }
}
