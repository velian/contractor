﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public GameObject m_ragdoll;
    public float m_health;
    public float m_minimumDistance = 4;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        GameObject player = GameObject.Find("Player");
        float distance = Vector3.Distance(player.transform.position, transform.position);
        Vector3 lookAtPos = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);

        transform.LookAt(new Vector3(lookAtPos.x, player.transform.position.y, lookAtPos.z));

        if (distance < m_minimumDistance)
            GetComponentInChildren<WeaponBase>().Fire();

        if(m_health <= 0)
        {
            if(GetComponentInParent<Room>())
            {
                GetComponentInParent<Room>().m_inhabitants.Remove(this);
            }

            GameObject ragdoll = (GameObject)Instantiate(m_ragdoll, new Vector3(transform.position.x, m_ragdoll.transform.position.y, transform.position.z), transform.rotation);
            Destroy(this.gameObject);
        }
	}

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<Bullet>().m_owner != name)
        {
            m_health -= collision.gameObject.GetComponent<Bullet>().m_damage;
            collision.gameObject.GetComponent<Bullet>().DestroyBullet();
        }
    }

    //Reveal The Enemy
    private bool m_lerping = false;

    public void RevealEnemy()
    {
        StartCoroutine(ERevealEnemy());
    }

    public void HideEnemy(float _alphaMin)
    {
        StartCoroutine(EHideEnemy(_alphaMin));
    }

    IEnumerator ERevealEnemy()
    {
        m_lerping = true;

        Color color = GetComponentInChildren<SkinnedMeshRenderer>().material.color;

        for (float i = color.a; i < 1; )
        {
            i += Time.deltaTime;
            GetComponentInChildren<SkinnedMeshRenderer>().material.color = new Color(color.r, color.g, color.b, i);
            yield return null;
        }

        m_lerping = false;
        yield return null;
    }

    IEnumerator EHideEnemy(float _alphaMin)
    {
        m_lerping = true;

        Color color = GetComponentInChildren<SkinnedMeshRenderer>().material.color;

        if (color.a < _alphaMin)
        {
            for (float i = color.a; i < _alphaMin; )
            {
                i += Time.deltaTime;
                //foreach (Renderer obj in )
                GetComponentInChildren<SkinnedMeshRenderer>().material.color = new Color(color.r, color.g, color.b, i);
                yield return null;
            }
        }
        else
        {
            for (float i = color.a; i > _alphaMin; )
            {
                i -= Time.deltaTime;

                SkinnedMeshRenderer rend = GetComponentInChildren<SkinnedMeshRenderer>();
                rend.material.color = new Color(color.r, color.g, color.b, i);
                yield return null;
            }
        }

        m_lerping = false;
        yield return null;
    }
}
