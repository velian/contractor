﻿using UnityEngine;
using System.Collections;

public class GetPanels : MonoBehaviour {

    public static GameObject FindInactiveWithTag(string tag)
    {
        Object[] objs = Resources.FindObjectsOfTypeAll(typeof(GameObject));

        foreach (GameObject obj in objs)
        {
            if (obj.tag == tag)
            {
                return obj;
            }
        }

        return null;
    }
}
