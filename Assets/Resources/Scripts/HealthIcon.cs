﻿using UnityEngine;
using System.Collections;

public class HealthIcon : MonoBehaviour {

    public void PlayAddAnimation()
    {
        GetComponentInParent<Animator>().Play("Add");
    }

    public void PlayRemoveAnimation()
    {
        GetComponentInParent<Animator>().Play("Remove");
    }
}