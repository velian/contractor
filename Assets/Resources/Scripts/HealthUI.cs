﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HealthUI : MonoBehaviour {

    GameObject m_player;
    List<GameObject> m_healthList;

    public GameObject m_healthPrefab;
    public float m_offset = 0.25f;
    public int m_capacity = 6;
    public int m_maxHealth = 20;

    // Use this for initialization
    void Start()
    {
        m_player = GameObject.FindWithTag("Player");
        m_healthList = new List<GameObject>();
        InitializeHealth();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            RemoveHealth();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            AddHealth();
        }
    }

    public void InitializeHealth()
    {
        Vector3 offset = Vector3.zero;
        m_healthList.Clear();

        foreach (Transform child in transform)
        {
            if (child)
                Destroy(child.gameObject);
        }

        for (int i = 0; i < m_player.GetComponent<PlayerController>().m_health; i++)
        {
            GameObject health = (GameObject)Instantiate(m_healthPrefab);
            health.transform.SetParent(GetComponent<RectTransform>().transform);

            RectTransform rectTrans = health.GetComponent<RectTransform>();

            rectTrans.position = transform.position;
            rectTrans.anchoredPosition = new Vector2(0.6f, -1.33f);

            //Offset if we have a huge fucking magazine
            if (i < m_capacity)
                rectTrans.anchoredPosition += new Vector2(i % m_capacity * m_offset, 0);
            else
                rectTrans.anchoredPosition += new Vector2(i % m_capacity * m_offset, -2.47f);

            rectTrans.localScale = new Vector3(0.06f, 0.13f, 1f);
            health.GetComponentInChildren<HealthIcon>().PlayAddAnimation();

            m_healthList.Add(health);
        }
    }
    
    public void AddHealth()
    {
        if (m_healthList.Count < m_maxHealth)
        {
            GameObject health = (GameObject)Instantiate(m_healthPrefab);
            health.transform.SetParent(GetComponent<RectTransform>().transform);

            RectTransform rectTrans = health.GetComponent<RectTransform>();

            rectTrans.position = transform.position;
            rectTrans.anchoredPosition = new Vector2(0.6f, -1.33f);

            //Offset if we have a huge fucking magazine
            if (m_healthList.Count < m_capacity)
                rectTrans.anchoredPosition += new Vector2(m_healthList.Count % m_capacity * m_offset, 0);
            else
                rectTrans.anchoredPosition += new Vector2(m_healthList.Count % m_capacity * m_offset, -2.47f);

            rectTrans.localScale = new Vector3(0.06f, 0.13f, 1f);
            health.GetComponentInChildren<HealthIcon>().PlayAddAnimation();

            m_healthList.Add(health);
        }
    }

    public void RemoveHealth()
    {
        if (m_healthList.Count > 0)
        {
            m_healthList[m_healthList.Count - 1].GetComponentInChildren<HealthIcon>().PlayRemoveAnimation();
            m_healthList.RemoveAt(m_healthList.Count - 1);
        }
    }

}