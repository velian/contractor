﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LootGenerator : MonoBehaviour {

    public Sprite[] m_spriteList;
    public int m_spawnPointNumber = 4;
    public int m_minimumSpawnChance = -3;
    public int m_maximumSpawnChance = 3;
    List<Transform> m_spawnPoints;

    void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        if (GetComponentInParent<Room>().m_startingRoom == true) return;

        m_spawnPoints = new List<Transform>();
        string currentSpawnName;

        for (int i = 0; i < m_spawnPointNumber; i++)
        {
            currentSpawnName = string.Concat("Spawn", (i + 1));
            Transform spawn = transform.FindChild(currentSpawnName);
            m_spawnPoints.Add(spawn);
        }

        GameObject cratePrefab = Resources.Load<GameObject>("Prefabs/Crate");

        foreach (Transform spawn in m_spawnPoints)
        {
            int spawnRoll = Random.Range(m_minimumSpawnChance, m_maximumSpawnChance);

            if (spawnRoll > 0)
            {
                //Create a new crate object
                GameObject crate = (GameObject)Instantiate(cratePrefab, spawn.position, cratePrefab.transform.rotation);
                crate.transform.SetParent(this.transform);

                //Quickly give us one of the random sprites
                int spriteNumber = Random.Range(0, m_spriteList.Length);
                crate.GetComponent<SpriteRenderer>().sprite = m_spriteList[spriteNumber];
            }
        }
    }
}
