﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

    public static GameObject manager;

	// Use this for initialization
	void Start ()
    {
        if (manager == null)
        {
            DontDestroyOnLoad(gameObject);
            manager = gameObject;
        }
        else if(manager != gameObject)
        {
            Destroy(gameObject);
        }
	}
}
