﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float m_speed = 20;
    public int m_health = 20;

    GameObject m_charPanel;
    GameObject m_healthPanel;

    void Start()
    {
        m_charPanel = GetPanels.FindInactiveWithTag("EquipmentSystem");
        m_healthPanel = GameObject.Find("Health Panel");

        GameObject panel = GetPanels.FindInactiveWithTag("Pistol");
        if (panel.transform.childCount > 0)
        {
            Component[] storageThing = panel.transform.GetComponentsInChildren(typeof(ItemOnObject), true);
            Sprite icon = storageThing[0].GetComponent<ItemOnObject>().item.itemIcon;

            SetWeapon(6, 1, 1, 0.2f, 0.3f, 0.5f, icon, true);
        }
    }

	// Update is called once per frame
	void Update ()
    {
        HandleWeapon();

        Plane groundPlane = new Plane(Vector3.up, -2);

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float distance;
        Vector3 hitPos = new Vector3(0,0,0);

        if (groundPlane.Raycast(ray, out distance))
        {
            hitPos = ray.GetPoint(distance);
            hitPos.y = transform.position.y;
            transform.LookAt(hitPos);
        }
        
        Vector3 direction = new Vector3(0,0,0);

        bool bMoving = false;
	    if(Input.GetKey(KeyCode.W))
        {
            direction += Vector3.forward + Vector3.right;
            bMoving = true;
        }
        if (Input.GetKey(KeyCode.A))
        {
            direction += Vector3.left + Vector3.forward;
            bMoving = true;
        }
        if (Input.GetKey(KeyCode.S))
        {
            direction -= Vector3.forward - Vector3.left;
            bMoving = true;
        }
        if (Input.GetKey(KeyCode.D))
        {
            direction += Vector3.right - Vector3.forward;
            bMoving = true;
        }

        if(direction.magnitude > 0.0f)
        {
            direction.Normalize();

            float fForwardDot = Vector3.Dot(transform.forward, direction);
            GetComponent<Rigidbody>().AddForce(direction * Time.deltaTime * m_speed);

            bool forward = fForwardDot < 0 ? false : true;

            if(!forward)            
                fForwardDot = Mathf.Clamp01(-fForwardDot);            
            else
                fForwardDot = Mathf.Clamp01(fForwardDot);
            

            GetComponentInChildren<Animator>().SetBool("bForward", forward);
            GetComponentInChildren<Animator>().SetFloat("fMovingDot", fForwardDot);
        }

        GetComponentInChildren<Animator>().SetBool("bMoving", bMoving);     
	}

    void HandleWeapon()
    {
        //ItemType[] types = m_charPanel.GetComponent<EquipmentSystem>();

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {            
            GameObject panel = GetPanels.FindInactiveWithTag("Pistol");

            if (panel.transform.childCount > 0)
            {
                //SetWeapon Pistol;
                print("Weapon set to Pistol");

                Component[] storageThing = panel.transform.GetComponentsInChildren(typeof(ItemOnObject), true);
                Sprite icon = storageThing[0].GetComponent<ItemOnObject>().item.itemIcon;

                SetWeapon(6, 1, 1, 0.2f, 0.3f, 0.5f, icon, true);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            GameObject panel = GetPanels.FindInactiveWithTag("SMG");

            if (panel.transform.childCount > 0)
            {
                //SetWeapon Pistol;
                print("Weapon set to SMG");

                Component[] storageThing = panel.transform.GetComponentsInChildren(typeof(ItemOnObject), true);
                Sprite icon = storageThing[0].GetComponent<ItemOnObject>().item.itemIcon;

                SetWeapon(25, 2, 1, 0.3f, 0.05f, 0.5f, icon, true);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            GameObject panel = GetPanels.FindInactiveWithTag("Shotgun");

            if (panel.transform.childCount > 0)
            {
                //SetWeapon Pistol;
                print("Weapon set to Shotgun");

                Component[] storageThing = panel.transform.GetComponentsInChildren(typeof(ItemOnObject), true);
                Sprite icon = storageThing[0].GetComponent<ItemOnObject>().item.itemIcon;

                SetWeapon(12, 6, 1, 0.4f, 0.6f, 0.5f, icon, true);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            GameObject panel = GetPanels.FindInactiveWithTag("AssaultRifle");

            if (panel.transform.childCount > 0)
            {
                //SetWeapon Pistol;
                print("Weapon set to Assault Rifle");

                Component[] storageThing = panel.transform.GetComponentsInChildren(typeof(ItemOnObject), true);
                Sprite icon = storageThing[0].GetComponent<ItemOnObject>().item.itemIcon;

                SetWeapon(30, 1, 1, 0.1f, 0.1f, 0.5f, icon, true);
            }
        }
    }

    void SetWeapon(int clipSize = 10, int numberOfBullets = 1, int baseDamage = 1, float spreadMax = 0.2f, float fireCooldown = 0.2f, float reloadCooldown = 0.5f, Sprite icon = null, bool infiniteAmmo = false)
    {
        WeaponBase wepBase = GetComponentInChildren<WeaponBase>();

        wepBase.m_clipSize = clipSize;
        wepBase.m_clip = wepBase.m_clipSize;
        wepBase.m_numberOfBullets = numberOfBullets;
        wepBase.m_baseDamage = baseDamage;
        wepBase.m_spreadMax = spreadMax;
        wepBase.m_fireCooldown = fireCooldown;
        wepBase.m_reloadCooldown = reloadCooldown;
        wepBase.m_infiniteAmmo = infiniteAmmo;

        GameObject.FindWithTag("WeaponPanel").GetComponent<WeaponPanel>().SetImage(icon);
        if(GameObject.Find("Ammo Panel").GetComponent<AmmunitionUI>().m_bulletList != null)
            GameObject.Find("Ammo Panel").GetComponent<AmmunitionUI>().ClearChildren();

        wepBase.Reload();
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<Bullet>())
        {
            if(collision.gameObject.GetComponent<Bullet>().m_owner == name) return;

            Physics.IgnoreCollision(collision.gameObject.GetComponent<Collider>(), GetComponent<Collider>());

            for (int i = 0; i < collision.gameObject.GetComponent<Bullet>().m_damage; i++ )
                m_healthPanel.GetComponent<HealthUI>().RemoveHealth();

            Destroy(collision.gameObject);
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if(collision.GetComponent<Tile>())
        {
            if(collision.GetComponent<Tile>().m_containsConversation)
            {
                GetComponent<Animator>().SetBool("bMoving", false);
            }
        }
    }
}
