﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Room : MonoBehaviour
{
    public bool m_hideOnAwake = true;
    public bool m_startingRoom = false;

    protected bool m_gray = false;

    public Room[] m_roomConnections = null;
    public List<Enemy> m_inhabitants = null;
    protected Tile[] m_children = null;
    

    // Use this for initialization
    void Start()
    {
        m_children = GetComponentsInChildren<Tile>();

        if (m_inhabitants != null)
        {
            m_inhabitants = new List<Enemy>();
            m_inhabitants.AddRange(GetComponentsInChildren<Enemy>());
        }

        if (m_startingRoom || !m_hideOnAwake)
        {
            foreach (Tile obj in m_children)
            {
                obj.RevealWall();
            }

            if (m_roomConnections != null && m_startingRoom)
            {
                foreach (Room neighbor in m_roomConnections)
                {
                    if (!neighbor.m_gray)
                    {
                        if(neighbor.m_children == null)
                        {
                            neighbor.m_children = neighbor.GetComponentsInChildren<Tile>();
                        }

                        neighbor.HideWalls(0.4f);
                        neighbor.m_gray = true;
                    }
                }
            }
        }
        else
        {           
            if(!m_gray)
                HideWalls(0);
        }
    }


    public void RevealWalls()
    {
        if (m_children != null)
        {
            foreach (Tile tile in m_children)
            {
                tile.RevealWall();
            }
        }

        if (m_inhabitants != null)
        {
            foreach (Enemy enemy in m_inhabitants)
            {
                enemy.RevealEnemy();
            }
        }

        foreach (Room room in m_roomConnections)
        {
            room.HideWalls(0.4f);
            room.m_gray = true;
        }
    }

    public void HideWalls(float _alphaMin)
    {
        if (m_children != null)
        {
            foreach (Tile tile in m_children)
            {
                tile.HideWall(_alphaMin);
            }
        }

        if(m_inhabitants != null)
        {
            foreach (Enemy enemy in m_inhabitants)
            {
                enemy.HideEnemy(_alphaMin);
            }
        }
    }
}