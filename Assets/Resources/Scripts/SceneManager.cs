﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour
{
    public Texture m_black;

    public float m_speed = 1;
    protected float m_alpha = 1;
	
    void Start()
    {
        FadeIn();
    }

    void OnGUI()
    {
        GUI.color = new Color(0, 0, 0, m_alpha);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), m_black);
    }

    public void FadeIn()
    {
        StartCoroutine(EFadeIn());
    }

    public void FadeOut(int _sceneNumber = -1)
    {
        StartCoroutine(EFadeOut(_sceneNumber));
    }

    IEnumerator EFadeIn()
    {
        for (float i = 1; i > 0; )
        {
            i -= Time.deltaTime * m_speed;
            m_alpha = i;
            yield return null;
        }
        yield return null;
    }

    IEnumerator EFadeOut(int _sceneNumber = -1)
    {
        for (float i = 0; i < 1; )
        {
            i += Time.deltaTime * m_speed;
            m_alpha = i;
            yield return null;
        }

        if (_sceneNumber == -1)
            Application.Quit();
        else
            Application.LoadLevel(_sceneNumber);
    }
}