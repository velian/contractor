﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    public bool m_containsConversation = false;
    private bool m_lerping = false;

    public void RevealWall()
    {
        StartCoroutine(ERevealWall());
    }

    public void HideWall(float _alphaMin)
    {
        StartCoroutine(EHideWall(_alphaMin));
    }

    IEnumerator ERevealWall()
    {
        m_lerping = true;

        Color color = GetComponentInChildren<Renderer>().material.color;

        for (float i = color.a; i < 1; )
        {
            i += Time.deltaTime;
            foreach (SpriteRenderer obj in GetComponentsInChildren<SpriteRenderer>())
                obj.GetComponent<SpriteRenderer>().material.color = new Color(color.r, color.g, color.b, i);
            yield return null;
        }

        m_lerping = false;
        yield return null;
    }    

    IEnumerator EHideWall(float _alphaMin)
    {
        m_lerping = true;

        Color color = GetComponentInChildren<Renderer>().material.color;

        if (color.a < _alphaMin)
        {
            for (float i = color.a; i < _alphaMin; )
            {
                i += Time.deltaTime;
                foreach (SpriteRenderer obj in GetComponentsInChildren<SpriteRenderer>())
                    obj.GetComponent<SpriteRenderer>().material.color = new Color(color.r, color.g, color.b, i);
                yield return null;
            }
        }
        else
        {
            for (float i = color.a; i > _alphaMin; )
            {
                i -= Time.deltaTime;
                foreach (SpriteRenderer obj in GetComponentsInChildren<SpriteRenderer>())
                    obj.GetComponent<SpriteRenderer>().material.color = new Color(color.r, color.g, color.b, i);
                yield return null;
            }
        }

        m_lerping = false;
        yield return null;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GetComponentInParent<Room>().RevealWalls();
        }
    }
}
