﻿using UnityEngine;
using System.Collections;

public class WeaponBase : MonoBehaviour
{
    [System.Serializable]
    public class Attributes
    {
        public uint m_strength = 0;
        public uint m_perception = 0;
        public uint m_endurance = 0;
        public uint m_dexterity = 0;
        public uint m_agility = 0;
    }

    //Variables
    public GameObject m_model = null;
    public GameObject m_bulletPrefab = null;

    public Attributes m_attributes;

    public bool m_playerWeapon = false;
    public bool m_infiniteAmmo = false;

    public int m_clipSize = 0;
    public int m_clip = 0;
    public int m_ammunition = 0;

    public int m_numberOfBullets = 1;

    public Color m_bulletColor;    

    public float m_baseDamage = 1;

    public float m_spreadMax = 0.1f;

    public float m_fireCooldown = 0.5f;
    public float m_reloadCooldown = 0.5f;

    public string m_weaponName = null;

    protected bool m_reloading = false;
    protected bool m_firing = false;

    protected GameObject m_invMain;

	// Use this for initialization
	void Start ()
    {
        if (m_model == null)
        {
            print("Tried to initialzed weapon without model.");
        }

        if (m_bulletPrefab == null)
        {
            print("Tried to initialzed weapon without bullet prefab.");
        }

        if (m_weaponName == null)
        {
            print("Tried to initialize weapon with no name.");
        }

        m_invMain = GetPanels.FindInactiveWithTag("MainInventory");
	}
	
    void Update()
    {
        WeaponUpdate();
    }

	// Update is called once per frame
	public void WeaponUpdate()
    {
        if (m_playerWeapon)
        {
            if (Input.GetMouseButton(0))
            {
                if (m_playerWeapon && m_invMain.active == false)
                    Fire();
                else if (!m_playerWeapon)
                    Fire();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                Reload();
            }
        }
	}

    public void Fire()
    {
        if (m_firing || m_reloading) return;

        StartCoroutine(FireEnumerator());
    }

    IEnumerator FireEnumerator()
    {
        m_firing = true;

        if (m_clip > 0)
        {
            m_clip -= 1;

            GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);

            if (m_playerWeapon)
                GameObject.Find("Ammo Panel").GetComponent<AmmunitionUI>().RemoveBullet();

            for (int i = 0; i < m_numberOfBullets; i++)
            {
                Vector3 localForward = -transform.right;

                GameObject bullet;
                bullet = (GameObject)Instantiate((GameObject)m_bulletPrefab, transform.position + (localForward * (0.25f)), new Quaternion(1, 1, 1, 1));

                bullet.GetComponent<Bullet>().m_direction = localForward + new Vector3(Random.Range(-m_spreadMax, m_spreadMax), 0, Random.Range(-m_spreadMax, m_spreadMax));
                bullet.transform.LookAt(new Vector3(transform.position.x, 0, transform.position.z));

                if(m_playerWeapon)
                    bullet.GetComponent<Bullet>().m_owner = GetComponentInParent<Transform>().GetComponentInParent<Transform>().tag;
                else
                    bullet.GetComponent<Bullet>().m_owner = GetComponentInParent<Transform>().tag;

                bullet.GetComponent<Bullet>().m_damage = m_baseDamage + (m_attributes.m_perception + m_attributes.m_dexterity);
                bullet.GetComponent<ParticleSystem>().GetComponent<Renderer>().material.color = m_bulletColor;
            }
        }
        else if (m_clip == 0)
        {
            Reload();
        }        

        yield return new WaitForSeconds(m_fireCooldown);        

        m_firing = false;
    }

    public void Reload()
    {
        //If we're reloading already, don't need to run any reload stuff
        if (m_reloading) return;
        
        StartCoroutine(ReloadEnumerator());
    }

    IEnumerator ReloadEnumerator()
    {
        m_reloading = true;

        yield return new WaitForSeconds(m_reloadCooldown);

        for (int i = m_clip; i < m_clipSize; i++)
        {
            if (m_clip != m_clipSize && m_ammunition != 0 || m_infiniteAmmo)
            {
                m_clip++;
                m_ammunition = (m_infiniteAmmo) ? 0 : m_ammunition - 1;
                continue;
            }
        }

        if (m_playerWeapon)
        {
            GameObject.Find("Ammo Panel").GetComponent<AmmunitionUI>().ClearChildren();
            GameObject.Find("Ammo Panel").GetComponent<AmmunitionUI>().InitializeClip();
        }

        m_reloading = false;
    }
}
