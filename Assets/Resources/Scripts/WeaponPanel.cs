﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponPanel : MonoBehaviour {

	public void SetImage(Sprite _sprite)
    {
        GetComponent<Image>().sprite = _sprite;
    }
}
